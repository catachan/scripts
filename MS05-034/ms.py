import sys
import urllib2
from colorconsole import terminal

screen = terminal.get_terminal(conEmu=False)
screen.clear()

file = open('liste.txt')
for line in iter(file):
	if len(sys.argv) == 2:
		if sys.argv[1] == "-s":
			url = 'https://' + line
		else:
			url = 'http://' + line
	else:
		url = 'http://' + line
		
	print "###### Checking " + url + "\n"
	
	request = urllib2.Request(url)
	request.add_header('Range', 'bytes=0-18446744073709551615')
	opener = urllib2.build_opener()
	counter = 0
	try:
		feeddata = opener.open(request).read()
	except Exception, e:
			if "Requested Range Not Satisfiable" in str(e):
					screen.cprint(4,0, "[!] Server appears to be vulnerable - got requested 'Request Range Not Satisfiable'.\n")
					screen.reset()
					counter = 1
	 
			else:
					screen.cprint(15,0, "[*] Does not appear to be vulnerable or got a different response. Printing response: " + str(e) + "\n")
					screen.reset()
					counter = 1
file.close()
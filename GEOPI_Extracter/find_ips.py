import os
import re 
import sys
import pygeoip
from IPy import IP

gi = pygeoip.GeoIP('GeoIP.dat')
logfilepath = sys.argv[1]

for filename in os.listdir(logfilepath):
	file = open(logfilepath+'\\'+filename, "r")

	for text in file.readlines():
		for m in re.finditer('(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})\.(?:[\d]{1,3})', text):
			ip = IP(m.group(0))
			if ip.iptype() is not 'PRIVATE':
				sys.stdout.write("%s %s\n" % (m.group(0),gi.country_name_by_addr(m.group(0))))

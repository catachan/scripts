########################################################
### Script to create O365 rules cisco 				####
### by mmfeu (mann&mouse IT services GmbH)			####
### 07.04.2016	                      				####
###	V2: Removed external code for CIDR				####
### V3: Argument for Platform						####
### V4: Dynamic Object name (date)					####
########################################################

$platform=$args[0]

if ($platform -notmatch "^ASA$|^IOS$")
{
	write-host "Error: Argument missing"
	write-host "Usage: office365_ips.ps1 [ASA|IOS]"
}
else
{
	##### Downloading the XML File
	[xml]$o365ips = (Invoke-webrequest -URI "https://support.content.office.net/en-us/static/O365IPAddresses.xml").Content

	##### define all needed products from XML => o365 always needed
	$products = @("o365","LYO","identity")

	##### Generating Content  ######
	$date = get-date -f ddMMyyyy
	write-host "object-group network office365_"$date
	Foreach ( $x in $products)
	{
		$o365base = @($o365ips.products.product | Where-Object {$_.name -eq $x} | select -ExpandProperty childnodes | Where-Object {$_.type -eq "IPv4"} | select -ExpandProperty childnodes )
		write-host "! ##### " $x " ######"
		Foreach ( $i in $o365base)
		{
			$ip = $i."#text".ToString()
			if ($ip -match "/") 
			{
				$sm = $ip.Substring($ip.IndexOf("/")+1)
				
				switch ($sm)
				{
					30 {$sm="255.255.255.252"}
					29 {$sm="255.255.255.248"}
					28 {$sm="255.255.255.240"}
					27 {$sm="255.255.255.224"}
					26 {$sm="255.255.255.192"}
					25 {$sm="255.255.255.128"}
					24 {$sm="255.255.255.0"}
					23 {$sm="255.255.254.0"}
					22 {$sm="255.255.252.0"}
					21 {$sm="255.255.248.0"}
					20 {$sm="255.255.240.0"}
					19 {$sm="255.255.224.0"}
					18 {$sm="255.255.192.0"}
					17 {$sm="255.255.128.0"}
					16 {$sm="255.255.0.0"}
					15 {$sm="255.254.0.0"}
					14 {$sm="255.252.0.0"}
					13 {$sm="255.248.0.0"}
					12 {$sm="255.240.0.0"}
					11 {$sm="255.224.0.0"}
					10 {$sm="255.192.0.0"}
					9 {$sm="255.128.0.0"}
					8 {$sm="255.0.0.0"}
					7 {$sm="254.0.0.0"}
					6 {$sm="252.0.0.0"}
					5 {$sm="248.0.0.0"}
					4 {$sm="240.0.0.0"}
					3 {$sm="224.0.0.0"}
					2 {$sm="192.0.0.0"}
					1 {$sm="128.0.0.0"}
					0 {$sm="0.0.0.0"}
					default {"Error"}
				}
				if ($platform -eq "ASA")
				{
					write-host "network-object" $ip.Substring(0,$ip.IndexOf("/") ) $sm
				}
				else
				{
					write-host $ip.Substring(0,$ip.IndexOf("/") ) $sm
				}
			}
			else	
			{
				if ($platform -eq "ASA")
				{
					write-host "network-object host" $ip
				}
				else
				{
					write-host "host" $ip
				}
			}
		}
	}
	write-host "! ##### DONE ######"
}

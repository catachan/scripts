import sys
import os
import re 
from netaddr import *

#pip install netaddr

if len(sys.argv) < 3:
    sys.stderr.write('Usage: ' + sys.argv[0] + ' [OBJECTNAME] [IPFILE]')
    sys.exit(1)
 
print ("object-group network " + sys.argv[1])

f = open(sys.argv[2], 'r')
for ips in f:
	ip = IPNetwork(ips)
	print("network-object " + str(ip.network) + " " +str(ip.netmask))

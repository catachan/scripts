#include <MsgBoxConstants.au3>

Local $sVar = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers", "DefaultLevel")

If $sVar = 0 Then
	Local $errorcode = RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers", "DefaultLevel", "REG_DWORD", "262144")
    If $errorcode = 1 Then
		MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "SRP disabled")
		sleep(900000)
		;Reset afer 15 Minutes
		Local $errorcode2 = RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers", "DefaultLevel", "REG_DWORD", "00000000")
		If $errorcode2 = 1 Then
			MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "SRP enabled again")
		else
			MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "ERROR ENABLING SRP again")
		EndIf	
	else
		MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "ERROR DISABLING SRP")
	EndIf
else
	Local $errorcode = RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Safer\CodeIdentifiers", "DefaultLevel", "REG_DWORD", "00000000")
	If $errorcode = 1 Then
		MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "SRP enabled")
	else
		MsgBox($MB_SYSTEMMODAL, "mann&mouse SRP TOOL", "ERROR ENABLING SRP")
	EndIf
EndIf
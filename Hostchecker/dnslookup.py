import socket
import sys
from socket import gethostbyname, gaierror
#print("***********************************")
#print("*  DNS Lookup Tool                *")
#print("*  paul.freitag@mannundmouse.com  *")
#print("*  Copyright 2013                 *")
#print("***********************************")
#print("")

if len(sys.argv) < 3:
    sys.stderr.write('Usage: ' + sys.argv[0] + ' [URL] [HOSTFILE]')
    sys.exit(1)
    
domain_input = sys.argv[1]
domain = "."+domain_input
f = open(sys.argv[2], 'r')
for host in f:
    host=host.replace("\n", "")
    hostname = host+domain
    try:
        ip=socket.gethostbyname(hostname)
        print(hostname + ";" + ip)
    except socket.gaierror:
        pass
#input("Press Enter to Continue...")
